Persisted scratchpads accessible via command palette.

# Usage

`Commnand+shift+p` and search for "open scratchpad"

![Usage demo](https://gitlab.com/msapka/nova-scratchpad/-/raw/77c20207c50bb49e55b0e1952e69e3ca395586e5/example.gif)
  
# Acknowledgments

Icon made by Pixel perfect from www.flaticon.com