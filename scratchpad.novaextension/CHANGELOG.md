## Version 0.3.1

Moved the command to command palette (from editor)
Added an extension command

## Version 0.3

Added support for multiple scrathpads

## Version 0.2

Added configuration for file location

## Version 0.1

Initial release
